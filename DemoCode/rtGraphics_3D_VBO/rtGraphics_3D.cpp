// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"
#include "src/GURiffModel.h"
#include "al.h"
#include "alc.h"
#include "efx.h"
#include "EFX-Util.h"
#include "efx-creative.h"
#include <xram.h>
#include <mmreg.h>


using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int								mouse_x, mouse_y;
bool							mDown = false;
bool							wKeyDown,sKeyDown, eKeyDown, dKeyDown, qKeyDown, aKeyDown = false;

GUClock* mainClock = nullptr;

//
// Main scene resources
//
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;

const aiScene* aiBeast;
const aiScene* aiTank;
const aiScene* aiFloor;

//GLuint							beastTexture;
GLuint tankTexture;
GLuint floorTexture;

//sceneVBOs* Beast;
sceneVBOs* Tank;
sceneVBOs* Floor;

GLuint meshShader;


float playerX = 4.0;
float playerY = -0.5;
float playerZ = -2.0;



#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("3D Example 01");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(0.0f, 0.0f, 15.0f, 55.0f, viewportAspect, 0.1f);

	principleAxes = new CGPrincipleAxes();

	texturedQuad = new CGTexturedQuad("textures\\wine-glass.png");
	
	//beast
	//aiBeast = aiImportModel(string("beast.obj"),
	//	aiProcess_GenNormals |
	//	aiProcess_CalcTangentSpace |
	//	aiProcess_Triangulate |
	//	aiProcess_JoinIdenticalVertices |
	//	aiProcess_SortByPType);

	//Beast = new sceneVBOs(aiBeast);
	//beastTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\beast_texture.bmp", TextureProperties(false));
	
	//tank
	aiTank = aiImportModel(string("models\\tankk.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Tank = new sceneVBOs(aiTank);
	tankTexture = fiLoadTexture("textures\\HullBakedD5.png", TextureProperties(false));

	//floor
	aiFloor = aiImportModel(string("models\\floor.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Floor = new sceneVBOs(aiFloor);
	floorTexture = fiLoadTexture("textures\\floor.jpeg", TextureProperties(false));
	
	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	///////////////
	//Sound setup//
	//////////////

	ALCdevice* alcDevice = alcOpenDevice(NULL);
	ALCcontext* alcContext = nullptr;

	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);

	ALuint Mybuffer;
	alGenBuffers(1, &Mybuffer);

	auto myRIFFSound = new GURiffModel("audio\\Theme.wav");

	RiffChunk formatChunk = myRIFFSound->riffChunkForKey('tmf');
	RiffChunk dataChunk = myRIFFSound->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);

	alBufferData(
		Mybuffer,
		AL_FORMAT_MONO16,
		(ALvoid*)dataChunk.data,
		(ALsizei)dataChunk.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);

	ALuint source1;

	alGenSources(1, &source1);

	//Attach buffer1 to source1
	alSourcei(source1, AL_BUFFER, Mybuffer);

	alSource3f(source1, AL_POSITION, 10.0F, 0.0F, 0.0F);
	alSource3f(source1, AL_VELOCITY, 0.0f, 0.0f, 0.0f);//change these bits
	alSource3f(source1, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	auto cameraLocation = mainCamera->cameraLocation();
	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerOri[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };

	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listenerOri);

	alSourcePlay(source1);

	/////////////////////////////////
	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "CIS5013. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	if (wKeyDown == true) //forward
	{
		playerZ = -0.3f + playerZ;
		wKeyDown = false;
	}

	if (aKeyDown == true) 
	{
		playerX = -0.3f + playerX;
		aKeyDown = false;
	}

	if (dKeyDown == true)
	{
		playerX = 0.3f + playerX;
		dKeyDown = false;
	}
	
	if (sKeyDown == true)
	{
		playerZ = 0.3f + playerZ;
		sKeyDown = false;
	}

	if (eKeyDown == true)
	{
		playerY = 0.3f + playerY;
		eKeyDown = false;
	}

	if (qKeyDown == true)
	{
		playerY = -0.3f + playerY;
		qKeyDown = false;
	}

}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(-playerX,-playerY,-playerZ);

	if (principleAxes)
		principleAxes->render(T);

	//if (texturedQuad)
		//texturedQuad->render(T);

	// Render example model loaded from obj file
	/*glBindTexture(GL_TEXTURE_2D, beastTexture);
	glEnable(GL_TEXTURE_2D);
	aiRender(aiBeast, mainCamera);*/
	
	//glBindTexture(GL_TEXTURE_2D, beastTexture);
	//glEnable(GL_TEXTURE_2D);
	//static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	//glUseProgram(meshShader);
	//glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	//Beast->render();

	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");

	glBindTexture(GL_TEXTURE_2D, tankTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 modelMatrix = GUMatrix4::translationMatrix(playerX, playerY, playerZ) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5) * GUMatrix4::rotationMatrix(0.0, 3.14, 0.0);
	GUMatrix4 newT = T * modelMatrix; 
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();
	
	modelMatrix = GUMatrix4::translationMatrix(-4.0, 0.0, -2.0) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5); //* GUMatrix4::rotationMatrix(90.0, 0.0, 0.0);
	newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();

	modelMatrix = GUMatrix4::translationMatrix(-8.0, 0.0, -2.0) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5); //* GUMatrix4::rotationMatrix(90.0, 0.0, 0.0);
	newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();

	modelMatrix = GUMatrix4::translationMatrix(-16.0, 0.0, -2.0) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5); //* GUMatrix4::rotationMatrix(90.0, 0.0, 0.0);
	newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();

	modelMatrix = GUMatrix4::translationMatrix(-20.0, 0.0, -2.0) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5); //* GUMatrix4::rotationMatrix(90.0, 0.0, 0.0);
	newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();

	glBindTexture(GL_TEXTURE_2D, floorTexture);
	glEnable(GL_TEXTURE_2D);
	modelMatrix = GUMatrix4::translationMatrix(10.0, 30.0, 30.0)* GUMatrix4::scaleMatrix(0.5, 0.5, 0.5); //*GUMatrix4::rotationMatrix(0.0,0.0,0.0) to rotate
	newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Floor->render();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	GUMatrix4 modelMatrix2 =GUMatrix4::translationMatrix(0.0, 0.0, 2.0)* GUMatrix4::scaleMatrix(1, 1, 1);
	GUMatrix4 newT2 = T * modelMatrix2;
	texturedQuad->render(newT2);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera)
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);
		auto cameraLocation = mainCamera->cameraLocation();
		alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));

		cameraLocation.normalise();
		ALfloat orientaion[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
		alListenerfv(AL_ORIENTATION, orientaion); //allows for more 3D feel when moving around the scene

	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);
		
		auto cameraLocation = mainCamera->cameraLocation();
		alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));

		cameraLocation.normalise();
		ALfloat orientaion[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
		alListenerfv(AL_ORIENTATION, orientaion); //allows for more 3D feel when zooming in and out of the scene
	}

}


void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();
	
	if (key == 'w')
		wKeyDown = true;

	if (key == 'a')
		aKeyDown = true;

	if (key == 'd')
		dKeyDown = true;

	if (key == 's')
		sKeyDown = true;

	if (key == 'q')
		qKeyDown = true;

	if (key == 'e')
		eKeyDown = true;

}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion


#pragma endregion


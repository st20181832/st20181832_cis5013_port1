#version 460

uniform mat4 mvpMatrix;
uniform vec3 L;
uniform vec3 colour;
uniform vec3 LK;

layout (location=0) in vec3 vertexPos;
layout (location=1) in vec3 vertexTexCoord;
layout(location=6) in vec3 vertexNormal;

out vec3 texCoord;

out vec4 position;
out vec3 pixelNormal;

void main(void) {

    pixelNormal = vertexNormal;
    texCoord = vertexTexCoord;
    position = mvpMatrix * vec4(vertexPos, 1.0);
    gl_Position = position;
}